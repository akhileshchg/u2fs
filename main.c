/*
 * Copyright (c) 1998-2011 Erez Zadok
 * Copyright (c) 2009	   Shrikar Archak
 * Copyright (c) 2003-2011 Stony Brook University
 * Copyright (c) 2003-2011 The Research Foundation of SUNY
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation.
 */

#include "wrapfs.h"
#include <linux/module.h>

int check_branch(const struct path *path)
{
    /* XXX: remove in ODF code -- stacking unions allowed there */
	if (!strcmp(path->dentry->d_sb->s_type->name, WRAPFS_NAME))
		return -EINVAL;
	if (!path->dentry->d_inode)
		return -ENOENT;
	if (!S_ISDIR(path->dentry->d_inode->i_mode))
		return -ENOTDIR;
	return 0;
}

static int is_branch_overlap(struct dentry *dent1, struct dentry *dent2)
{
	struct dentry *dent = NULL;
    
	dent = dent1;
	while ((dent != dent2) && (dent->d_parent != dent))
		dent = dent->d_parent;

	if (dent == dent2)
		return 1;

	dent = dent2;
	while ((dent != dent1) && (dent->d_parent != dent))
		dent = dent->d_parent;

	return (dent == dent1);
}

int wrapfs_parse_options(char* raw_data, struct path* lower_path)
{
	int err = 0;
	char *optname;
	char *optarg;
	bool ltree = NULL;
	bool rtree = NULL;
	struct dentry *dent1, *dent2;
	/**lower_path = kzalloc(2*sizeof(struct path), GFP_KERNEL);
        if (!*lower_path) {
		printk(KERN_CRIT "wrapfs: read_super: out of memory\n");
		err = -ENOMEM;
		goto out;
	}*/
	BUG_ON(!lower_path);
	while ((optname = strsep(&raw_data, ",")) != NULL) {
		optarg = strchr(optname, '=');
		if (optarg)
			*optarg++ = '\0';
		if (!optarg) {
			printk(KERN_ERR "wrapfs: %s requires an arugment",
			optname);
			err = -EINVAL;
			goto out;
		}
		if (!strcmp("ldir", optname)) {
			if (!ltree) {
				ltree = true;
				err = kern_path(optarg, LOOKUP_FOLLOW | LOOKUP_DIRECTORY,&lower_path[0]);
				if (err) {
					printk(KERN_INFO "wrapfs: error accessing ldir : %d\n", err);
					goto out_putpath;
				}
				err = check_branch(&lower_path[0]); 
				if (err) {
					printk(KERN_ERR "Invalid left branch\n");
					goto out_putpath;
				}
			} else {
				printk(KERN_ERR "Multiple ldir options given\n");
				err = -EINVAL;
				goto out_putpath;
			}
		}
		if (!strcmp("rdir", optname)) {
                        if (!rtree) {
                                rtree = true;
                                err = kern_path(optarg, LOOKUP_FOLLOW | LOOKUP_DIRECTORY,&lower_path[1]);
                                if (err) {
                                        printk(KERN_INFO "wrapfs: error accessing ldir : %d\n", err);
                                        goto out_putpath;
                                }
				err = check_branch(&lower_path[1]);
				if (err) {
					printk(KERN_ERR "Invalid right branch\n");
					goto out_putpath;
				}
                        } else {
                                printk(KERN_ERR "Multiple ldir options given\n");
                                err = -EINVAL;
                                goto out_putpath;
                        }
                }	
	}
	if(!ltree || !rtree) {
		//UDBG;
		err = -EINVAL;
		printk (KERN_ERR "One or more directories not specified\n");
		goto out_putpath;
	} else {
		//UDBG;
		/*CHECK FOR OVERLAPS*/
		dent1 = lower_path[0].dentry;
		dent2 = lower_path[1].dentry;
		if (is_branch_overlap(dent1, dent2)) {
			printk(KERN_ERR "wrapfs: branches 1 and "
				"2 overlap\n");
			err = -EINVAL;
			goto out_putpath;
		}
		goto out;
	}
out_putpath:
	if (ltree)
		path_put(&lower_path[0]);
	if (rtree)
		path_put(&lower_path[1]);
/*out_pathfree:
	kfree(*lower_path);
	*lower_path = NULL;
	//UDBG;*/
out:
	return err;
}
/*
 * There is no need to lock the wrapfs_super_info's rwsem as there is no
 * way anyone can have a reference to the superblock at this point in time.
 */

static int wrapfs_read_super(struct super_block *sb, void *raw_data, int silent)
{
	int err = 0;
	struct super_block *lower_sb;
	struct path lower_path[2];
	struct inode *inode;
	int i = 0;

	if (!raw_data) {
		printk (KERN_ERR
			"wrapfs: ldir,rdir not specified\n");
		err = -EINVAL;
		goto out;
	}
	err = wrapfs_parse_options(raw_data, lower_path);
	if (err) {
		goto out;	
	}
	sb->s_fs_info = kzalloc(sizeof(struct wrapfs_sb_info), GFP_KERNEL);
	if (!WRAPFS_SB(sb)) {
		printk(KERN_CRIT "wrapfs: read_super: out of memory\n");
		err = -ENOMEM;
		goto out_free;
	} else {
		WRAPFS_SB(sb)->lower_sb[0] = NULL;
		WRAPFS_SB(sb)->lower_sb[1] = NULL;
	}
	for (i = 0; i < 2; i++) {
		lower_sb = lower_path[i].dentry->d_sb;
		atomic_inc(&lower_sb->s_active);
		wrapfs_set_lower_super(sb, lower_sb, i);
		if (i == 0) {
			sb->s_maxbytes = lower_sb->s_maxbytes;
		}
	}
	sb->s_time_gran = 1;
	sb->s_op = &wrapfs_sops;
	inode = wrapfs_iget(sb, &lower_path[0].dentry->d_inode, 0);
	if (IS_ERR(inode)) {
		err = PTR_ERR(inode);
		goto out_sput;
	}
	wrapfs_set_lower_inode(inode, lower_path[1].dentry->d_inode, 1);
	sb->s_root = d_alloc_root(inode);
	if (!sb->s_root) {
		err = -ENOMEM;
		goto out_iput;
	}
	d_set_d_op(sb->s_root, &wrapfs_dops);
	sb->s_root->d_fsdata = NULL;
	err = new_dentry_private_data(sb->s_root);
	if (err) {
		goto out_freeroot;
	}
	wrapfs_set_lower_path(sb->s_root, &lower_path[0], 0);
	wrapfs_set_lower_path(sb->s_root, &lower_path[1], 1);
	d_rehash(sb->s_root);
        if (!silent)
		printk(KERN_INFO
			"wrapfs: mounted both %s and %s sucessfully\n",
			lower_path[0].dentry->d_name.name,
			lower_path[1].dentry->d_name.name);
	goto out;	

out_freeroot:
	dput(sb->s_root);
out_iput:
	iput(inode);
out_sput:
	atomic_dec(&wrapfs_lower_super(sb, 0)->s_active);	
	atomic_dec(&wrapfs_lower_super(sb, 1)->s_active);
	kfree(WRAPFS_SB(sb));
	sb->s_fs_info = NULL;
out_free:
	path_put(&lower_path[0]);
	path_put(&lower_path[1]);
	UDBG;
out:
	return err;
}
/*static int wrapfs_read_super(struct super_block *sb, void *raw_data, int silent)
{
	int err = 0;
	struct super_block *lower_sb;
	struct path *lower_path = NULL;
	struct inode *inode;
	int i = 0;
	struct inode *lower_i[2] = {NULL};
	//UDBG;
	//if (!dev_name) {
	//	printk(KERN_ERR
	//	       "wrapfs: read_super: missing dev_name argument\n");
	//	err = -EINVAL;
	//	goto out;
	//}

	//err = kern_path(dev_name, LOOKUP_FOLLOW | LOOKUP_DIRECTORY,
	//		&lower_path);
	//if (err) {
	///	printk(KERN_ERR	"wrapfs: error accessing "
	//	       "lower directory '%s'\n", dev_name);
	//	goto out;
	//}
	printk(KERN_INFO "first\n");
	if (!raw_data) {
		printk(KERN_ERR
			"wrapfs: read_super: missing -o ldir,rdir arguments\n");
		err = -EINVAL;
		goto out;
	}
	lower_path = kzalloc(2*sizeof(struct path), GFP_KERNEL);
	if (!lower_path) {
		printk(KERN_CRIT "wrapfs: read_super: out of memory\n");
		err = -ENOMEM;
		goto out;
	}
	printk(KERN_INFO "Before parsing\n");
	err = wrapfs_parse_options(raw_data, lower_path);
	printk(KERN_INFO "After parsing\n");
	if (err) {
		goto out_path;
	}
	sb->s_fs_info = kzalloc(sizeof(struct wrapfs_sb_info), GFP_KERNEL);
	if (!WRAPFS_SB(sb)) {
		printk(KERN_CRIT "wrapfs: read_super: out of memory\n");
		err = -ENOMEM;
		goto out_free;
	}
	spin_lock_init(&WRAPFS_SB(sb)->slock);
	spin_lock(&WRAPFS_SB(sb)->slock);
	WRAPFS_SB(sb)->lower_sb = kcalloc(2, sizeof(struct super_block *), GFP_KERNEL);
	if (!WRAPFS_SB(sb)->lower_sb) {
		printk(KERN_CRIT "wrapfs: read_super: out of memory\n");
		err = -ENOMEM;
		goto out_sput;
	}
	//UDBG;
	for (i = 0; i < 2; i++) {
		lower_sb = lower_path[i].dentry->d_sb;
		atomic_inc(&lower_sb->s_active);
		wrapfs_set_lower_super(sb, lower_sb, i);
		WRAPFS_SB(sb)->count  = i;
		lower_i[i] = lower_path[i].dentry->d_inode;
	}
	//UDBG;
	sb->s_maxbytes = lower_path[0].dentry->d_sb->s_maxbytes;
	//UDBG;
	sb->s_time_gran = 1;

	sb->s_op = &wrapfs_sops;


	inode = wrapfs_iget(sb, &lower_i[0], 0);
	wrapfs_set_lower_inode(inode, lower_i[1], 1);
	(WRAPFS_I(inode)->count)++;
	//UDBG;
	if (IS_ERR(inode)) {
		err = PTR_ERR(inode);
		goto out_sdata;
	}
	//inode->i_mode = S_IFDIR |0755;
	sb->s_root = d_alloc_root(inode);
	//UDBG;
	if (!sb->s_root) {
		err = -ENOMEM;
		goto out_iput;
	}
	d_set_d_op(sb->s_root, &wrapfs_dops);
	//UDBG;
	sb->s_root->d_fsdata = NULL;
	err = new_dentry_private_data(sb->s_root);
	//UDBG;
	if (err)
		goto out_freeroot;
	atomic_set(&WRAPFS_D(sb->s_root)->count, -1);
	WRAPFS_D(sb->s_root)->lower_path = lower_path;
	for (i = 0; i < 2; i++) {
		//wrapfs_set_lower_path(sb->s_root, &lower_path[i], i);
		atomic_inc(&WRAPFS_D(sb->s_root)->count);
	}
	//UDBG;
	d_rehash(sb->s_root);
	if (!silent)
		printk(KERN_INFO
		       "wrapfs: mounted both %s and %s sucessfully\n",
		       lower_path[0].dentry->d_name.name,
		       lower_path[1].dentry->d_name.name);
	spin_unlock(&WRAPFS_SB(sb)->slock);
	//UDBG;
	goto out; // all is well

out_freeroot:
	dput(sb->s_root);
out_iput:
	iput(inode);
out_sdata:
	kfree(WRAPFS_SB(sb)->lower_sb);
	WRAPFS_SB(sb)->lower_sb = NULL;
out_sput:
	spin_unlock(&WRAPFS_SB(sb)->slock);
	kfree(WRAPFS_SB(sb));
	sb->s_fs_info = NULL;
out_free:
	path_put(&lower_path[0]);
	path_put(&lower_path[1]);
out_path:	
	kfree(lower_path);

out:
	//UDBG;
	return err;
}*/

struct dentry *wrapfs_mount(struct file_system_type *fs_type, int flags,
			    const char *dev_name, void *raw_data)
{
	printk(KERN_INFO "nodev call\n");
	//UDBG;
	return mount_nodev(fs_type, flags, raw_data,
				wrapfs_read_super);
}

static struct file_system_type wrapfs_fs_type = {
	.owner		= THIS_MODULE,
	.name		= WRAPFS_NAME,
	.mount		= wrapfs_mount,
	.kill_sb	= generic_shutdown_super,
	.fs_flags	= FS_REVAL_DOT,
};

static int __init init_wrapfs_fs(void)
{
	int err;

	pr_info("Registering wrapfs " WRAPFS_VERSION "\n");

	err = wrapfs_init_inode_cache();
	if (err)
		goto out;
	err = wrapfs_init_dentry_cache();
	if (err)
		goto out;
	err = register_filesystem(&wrapfs_fs_type);
out:
	if (err) {
		wrapfs_destroy_inode_cache();
		wrapfs_destroy_dentry_cache();
	}
	return err;
}

static void __exit exit_wrapfs_fs(void)
{
	wrapfs_destroy_inode_cache();
	wrapfs_destroy_dentry_cache();
	unregister_filesystem(&wrapfs_fs_type);
	pr_info("Completed wrapfs module unload\n");
}

MODULE_AUTHOR("Erez Zadok, Filesystems and Storage Lab, Stony Brook University"
	      " (http://www.fsl.cs.sunysb.edu/)");
MODULE_DESCRIPTION("Wrapfs " WRAPFS_VERSION
		   " (http://wrapfs.filesystems.org/)");
MODULE_LICENSE("GPL");

module_init(init_wrapfs_fs);
module_exit(exit_wrapfs_fs);
