Introduction
============
U2FS is a fan-out stackable unification file system. U2FS is very
similar to UnionFS of FSL and is developed using WrapFS as a baseline.
U2FS has two branches, the left (LB) and the right (RB). LB has
higher priority than RB.  RB should be assumed to be readonly: that
is, no
file there should be modified, as if it came, say, from a readonly
CDROM;
files should be modified in LB only.  Pictorially, this could be seen
as
follows:

                u2fs
                 /\
                /  \
               /    \
              LB    RB

Design of U2FS
=============
The main challenge is to maintain POSIX semantics while still
maintaining the unified recursive view to two underlying paths.

1. For lookup, LB is searched first and if not find, the RB is
   searched.
2. When a new file is created, it is created in LB. Of course, it'll
   have to create the whole directory path (of RB) sometimes.
3. When a file in LB gets modified, the changes happen as is. But if
   file in RB gets modified, its first copied to LB and then the
   modifications will take place. This process is called "copy up".
4. If a file in RB is deleted, a corresponding "whitestone" or
   negative cache entry is created in LB. When a "whitestone" is
   listed its corresponding file in RB won't be returned in lookup.
5. In lookup sometimes duplicates are may be present in LB and RB. A
   hash is used to first understand the unique entities and only they
   are returned to user.
6. Inode numbers are generated dynamically using iunique. 

Using U2FS
==========
1. Install the kernel module for U2FS.
2. Mount using the command:

	
	mount -t u2fs -o ldir=/left/dir,rdir=/right/dir null /mnt/u2fs
